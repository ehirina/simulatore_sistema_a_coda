import math

class parametersCalc:
    def __init__(self, lambda_parameter, mu_parameter, Y):
        self.Y = Y
        self.lambda_parameter = lambda_parameter
        self.mu_parameter = mu_parameter
        self.a = lambda_parameter/mu_parameter

    def calc_Pk(self, k):
        p0 = 0
        for i in range(0, self.Y + 1):
            p0 += (self.a ** i)/math.factorial(i)
        p0 = p0 ** (-1)
        return ((self.a ** k)/math.factorial(k)) * p0

    def calc_BdiErlang(self):
        return self.calc_Pk(self.Y)

    def calc_Ek(self):
        return self.a * (1 - self.calc_BdiErlang())

    def calc_Et(self):
        return 1/self.mu_parameter

    def printResults(self):
        b = self.calc_BdiErlang()
        print("B(Y, A) = {:4.10f}".format(b))
        Ek = self.calc_Ek()
        print("Numero medio utenti nel sistema: {}".format(Ek))
        Et = self.calc_Et()
        print("Numero medio di permanenza nel sistema: {}".format(Et))


newCase = parametersCalc(125, 2, 100)
newCase.printResults()

