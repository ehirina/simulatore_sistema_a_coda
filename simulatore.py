import sys
import time

import numpy


class Simulatore:
    def __init__(self, lambda_parameter, mu_parameter, Y):
        self.MAX_PKG_IN_SYSTEM = Y
        self.num_in_system = 0
        self.current_time_instance = 0.0
        self.event_data = {}
        self.lambda_parameter = lambda_parameter
        self.mu_parameter = mu_parameter
        self.next_arrival_time = self.generate_arrival()
        self.departures = []
        self.departures.append(float('inf'))

        self.total_time_spent_in_system = 0.0
        self.num_arrivals = 0
        self.num_departs = 0
        self.blocks = 0

    def advance_time(self):
        min_departure = min(self.departures)

        self.current_time_instance = min(self.next_arrival_time, min_departure)

        if self.next_arrival_time < min_departure:
            self.handle_arrival_event(self.next_arrival_time)
        else:
            self.handle_departure_event(min_departure)

    def handle_arrival_event(self, arrival_time):
        self.next_arrival_time = self.current_time_instance + self.generate_arrival()
        if self.num_in_system + 1 > self.MAX_PKG_IN_SYSTEM:
            self.blocks += 1
            print("INFO:Package lost, System full")
            return

        self.num_arrivals += 1
        self.num_in_system += 1
        next_departure_time = self.current_time_instance + self.generate_departure()
        self.total_time_spent_in_system += next_departure_time - arrival_time
        self.departures.append(next_departure_time)

    def handle_departure_event(self, departure_event):
        self.num_in_system -= 1
        self.num_departs += 1

        self.departures.remove(departure_event)

    def generate_arrival(self):
        return numpy.random.exponential(1.0 / self.lambda_parameter)

    def generate_departure(self):
        return numpy.random.exponential(1.0/self.mu_parameter)


simulatore_mmyy = Simulatore(7, 5, 8)
clock_counter = 0
num_pkg_counter = 0

while True:
    clock_counter += 1
    simulatore_mmyy.advance_time()
    num_pkg_counter += simulatore_mmyy.num_in_system
    print("CLOCK :{}, num pkg in system: {}, total arrivals: {}, total departures: {}, media pkg in system: {:4.10f}, tempo medio di permanenza nel sistema: {}"
          .format(clock_counter, simulatore_mmyy.num_in_system, simulatore_mmyy.num_arrivals, simulatore_mmyy.num_departs,
                  num_pkg_counter/clock_counter, simulatore_mmyy.total_time_spent_in_system/simulatore_mmyy.num_arrivals))
    if clock_counter == 100000:
        break
    time.sleep(0.1)

print("Number of blocks: {} ".format(simulatore_mmyy.blocks))
